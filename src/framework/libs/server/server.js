const spdy = require("spdy");
const http = require("http");
const fsUtil = require("fs");
const pathUtil = require("path");
const debug = require("debug");

const serverLog = debug("app").extend("system").extend("server");

const KEY_FILE = "server.key";
const CERTIFICATE_FILE = "server.crt";

const sslOptions = () => {
  const sslPath = pathUtil.resolve("./resources/ssl");
  const keyPath = pathUtil.join(sslPath, KEY_FILE);
  const certificatePath = pathUtil.join(sslPath, CERTIFICATE_FILE);

  return {
    key: fsUtil.readFileSync(keyPath),
    cert: fsUtil.readFileSync(certificatePath)
  };
};

const selectServer = secure => app => {
  if (secure) return spdy.createServer(sslOptions(), app);
  return http.createServer(app);
}

const createServer = (app, wss, { port, host, secure }, cb) => {
  const server = selectServer(secure)(app);

  if (wss) server.on("upgrade", wss.upgrade);

  return {
    start() {
      const PORT = Number(port);
      const HOST = String(host);

      server.listen(PORT, HOST, async () => {
        await Promise.resolve(cb());
        serverLog("Server started");
      });
    },
    stop() {
      server.close(() => serverLog("Server stopped"));
    }
  }
};

module.exports = { createServer };