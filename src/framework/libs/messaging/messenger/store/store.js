const debug = require("debug");
const { createDatabase } = require("./database");
const { createPublisher } = require("./publisher");
const { createSubscriber } = require("./subscriber");

const storeDebug = debug("app").extend("system").extend("messaging").extend("store");

const createStore = ({
  readClient,
  writeClient
}, {
  onMessage,
  onClose
}, messengerConfig) => {
  const database = createDatabase(writeClient, messengerConfig);
  const publisher = createPublisher(writeClient, messengerConfig);

  const handleUnicast = async (type, payload, user) => {
    const userSocket = await database.findForUser(user);

    if (userSocket === null) return; // No Connection found

    onMessage({ type, payload, to: [userSocket] });

    storeDebug(`Unicast to User "${user}" (${userSocket}):`, type, payload);
  };

  const handleBroadcast = async (type, payload, room) => {
    const roomSockets = await database.findForRoom(room);

    if (roomSockets === null) return; // No Connections found

    onMessage({ type, payload, to: roomSockets });

    storeDebug(`Broadcast to Room "${room}" (${roomSockets}):`, type, payload);
  };

  const handleAnycast = async (type, payload) => {
    onMessage({ type, payload, to: null });

    storeDebug(`Anycast:`, type, payload);
  };

  // Message is received from PubSub
  const messageReceived = ({ type, payload, ...details }) => {
    // Unicast
    if (details.user) return handleUnicast(type, payload, details.user);

    // Multicast: UNNECESSARY AND COMPLEX - Just Broadcast instead OR Unicast multiple times
    //   + TOO LOGIC-SPECIFIC IF THERE WAS NEED FOR IT

    // Broadcast
    if (details.room) return handleBroadcast(type, payload, details.room);

    // Anycast
    return handleAnycast(type, payload);
  };

  const socketClosed = ({ socket }) => onClose(socket);

  const infoReceived = info => storeDebug(info);

  const subscriber = createSubscriber(readClient, {
    onMessage: messageReceived,
    onClose: socketClosed,
    onInfo: infoReceived,
  }, messengerConfig);

  const connect = async (socketId, userId, roomId) => {
    await database.save({ socketId, userId, roomId })
  };

  const send = async message => {
    await publisher.message(message);
  };

  const disconnectSocket = async socketId => {
    publisher.close(socketId);

    await database.remove(socketId);
  };

  const disconnectUser = async userId => {
    const socketId = await database.findForUser(userId);

    if (!socketId) return null;

    await disconnectSocket(socketId);

    return socketId;
  };

  const initialize = async () => {
    subscriber.initialize();
  }

  return {
    connect,
    send,
    disconnectSocket,
    disconnectUser,
    initialize,
  };
};

module.exports = { createStore };
