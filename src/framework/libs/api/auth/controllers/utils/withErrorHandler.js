const DEFAULT_STATUS = 500;

const DEFAULT_ERROR = { error: "Internal Error" };

// Handling raw errors
const withErrorHandler = controller => () => (req, res, next) =>
  Promise.resolve(controller(req, res, next)).catch(({ status, data }) => {
    return res.status(status || DEFAULT_STATUS).json(data || DEFAULT_ERROR);
  });

module.exports = { withErrorHandler }; 
