const { withErrorHandler } = require("./utils/withErrorHandler");

const createSignUpController = ({
  createUser,
  encodeToken,
}) => {
  const signup = async (req, res) => {
    const data = {
      ...req.body,
      image: req.file ? req.file.filename : null,
    };

    const user = await Promise.resolve(createUser(data));

    const token = await Promise.resolve(encodeToken(user));

    return res.status(200).json({ user, token });
  };

  return {
    path: "/signup",
    middleware: [
      {
        name: "upload",
        params: {
          field: "image"
        }
      }
    ],
    use: {
      method: "POST",
      controller: withErrorHandler(signup),
    }
  }
};

module.exports = { createSignUpController };
