const { withErrorHandler } = require("./utils/withErrorHandler");

const createSignInController = ({
  findUser,
  encodeToken,
}) => {
  const signin = async (req, res) => {
    const user = await Promise.resolve(findUser(req.body));

    const token = await Promise.resolve(encodeToken(user));

    return res.status(200).json({ user, token });
  };

  return {
    path: "/signin",
    schema: "signin",
    use: {
      method: "POST",
      controller: withErrorHandler(signin),
    }
  }
};

module.exports = { createSignInController };
