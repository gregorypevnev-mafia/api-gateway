const { extractToken } = require("./http");
const { encodeToken, decodeToken, invalidateToken } = require("./tokens");
const { defaultVerification } = require("./permissions");
const { createStore } = require("./store");

const createAuthUtils = ({
  tokens,
  http,
  store
}) => {
  const tokenStore = createStore(store);

  return {
    extractToken: extractToken(http),
    encodeToken: encodeToken(tokens),
    decodeToken: decodeToken(tokenStore, tokens),
    invalidateToken: invalidateToken(tokenStore),
    defaultVerifyPermissions: defaultVerification,
  };
};

module.exports = { createAuthUtils };