exports.extractToken = ({
  header,
  token,
}) => {
  const tokenPattern = new RegExp(`^${token} (.+)$`);

  return req => {
    const authHeader = String(req.headers[header.toLowerCase()] || ""); // Reminder: Express transforms all Headers to Lowercase

    if (!authHeader) throw { message: `"${header}" header not provided` };

    const match = authHeader.match(tokenPattern);

    if (!match || match.length < 2) throw { message: "Invalid type of token" };

    const token = match[1];

    if (!token) throw { message: "Token not found" };

    return token;
  };
}