const jwt = require("jsonwebtoken");

const invalidatedToken = token => `INVALIDATED_TOKEN_${token}`;

exports.encodeToken = ({
  algorithm,
  audience,
  issuer,
  secret,
  session
}) => user => {
  try {
    return jwt.sign({ user }, secret, {
      expiresIn: session,
      algorithm,
      audience,
      issuer,
      subject: String(user.id)
    }).toString();
  } catch (e) {
    return null;
  }
};

exports.decodeToken = (store, {
  algorithm,
  issuer,
  secret,
}) => async token => {
  const result = await store.get(invalidatedToken(token));

  if (result) throw { message: "Token invalidated" };

  try {
    const data = jwt.verify(token, secret, {
      algorithms: [algorithm],
      issuer
    });

    return data ? data.user : null;
  } catch (e) {
    if (e.name === "TokenExpiredError") throw { message: "Token expired" };

    return null;
  }
};

exports.invalidateToken = store => async token => {
  await store.set(invalidatedToken(token), true);
};