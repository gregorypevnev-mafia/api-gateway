const { createDefaultServer } = require("./server");
const { combineComponents } = require("./components");
const { createRoutes } = require("./routes");
const { errorHandler } = require("./errors");

const createApi = (components, dependencies, createServer, {
  apiPath,
  wsPath,
  ...config
}) => {
  const app = createServer ? createServer(config) : createDefaultServer();
  const { api, ws, middleware } = combineComponents(components, dependencies);

  app.use(apiPath, createRoutes(api, middleware, dependencies));
  app.use(wsPath, createRoutes(ws, middleware, dependencies));

  // Error-Handler: MUST BE LAST
  app.use(errorHandler);

  return app;
};

module.exports = { createApi };