const { createFilesController } = require("./controller");
const { createFilesMiddleware } = require("./middleware");
const { createDownloader } = require("./download");
const { createSignatureGenerator } = require("./signer");

const createFiles = ({
  secret,
  path,
  ttl,
  allowedExtensions,
  fileField,
}) => {
  const downloader = createDownloader(path);
  const signer = createSignatureGenerator({ secret, ttl, allowedExtensions });

  const fileController = createFilesController({ fileField }, { signer, downloader });
  const fileMiddleware = createFilesMiddleware({ path, fileField }, { signer });

  return {
    controller: fileController,
    middleware: [fileMiddleware]
  };
};

module.exports = { createFiles };
