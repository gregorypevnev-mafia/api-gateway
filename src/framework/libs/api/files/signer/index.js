const { createSigner } = require("./sign");
const { createVerifier } = require("./verify");

const createSignatureGenerator = ({ secret, ttl, allowedExtensions }) => {
  const sign = createSigner({
    secret,
    ttl,
    allowedExtensions,
  });

  const verify = createVerifier({ secret });

  return {
    sign,
    verify,
  };
};

module.exports = {
  createSignatureGenerator
}
