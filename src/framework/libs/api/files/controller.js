const toURL = ({ signature, expires }) => `/api/files?signature=${signature}&expires=${expires}`;

const createFilesController = ({ fileField }, { signer, downloader }) => {
  const signURL = ({ }) => (req, res) => {
    const { file } = req.body;
    // Note: Would also validate size in production application - Requires reading the entire file (TOO EXPENSIVE FOR NODEJS)
    //   - Would NOT be using Streams (WHICH IS SUPER-BAD)
    const user = req.token;

    try {
      const { signature, expires } = signer.sign(file, user);

      return res.status(200).json({
        url: toURL({ signature, expires }),
        field: fileField,
      });
    } catch (e) {
      return res.status(400).json({ message: e.message });
    }
  };

  const upload = ({ }) => (req, res) => {
    const filename = req.file.filename;

    return res.status(200).json({ filename });
  }

  const download = ({ }) => async (req, res) => {
    const readStream = await downloader.stream(req.params.filename);

    return readStream.pipe(res);
  };

  return {
    path: "/files",
    use: [
      {
        path: "/sign",
        use: {
          method: "POST",
          controller: signURL,
        },
        auth: true,
        private: true,
        schema: "file-upload",
      },
      {
        middleware: [
          {
            name: "file-upload",
            params: {},
          }
        ],
        use: {
          method: "POST",
          controller: upload
        },
        auth: true,
        private: true,
      },
      {
        path: "/:filename",
        use: {
          method: "GET",
          controller: download,
        }
        // Public - Avatar should be read by other users
      }
    ]
  };
};

// IMPORTANT: Only using Multer because FormData is universal for File-Upload (Safer for Mobile)

module.exports = { createFilesController };