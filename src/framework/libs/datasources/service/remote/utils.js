const isError = error => {
  if (error.response) return false; // Error-Response returned by server - NOT a Service-Error
  if (error.request) return true; // No response arrived -> Error
  return true;
};

module.exports = { isError };
