const { createProxyMiddleware } = require("http-proxy-middleware");

const PROXY_TIMEOUT = 5000;

const prefixPath = path => {
  const lastChar = path.length - 1;

  return path[lastChar] === '/' ? path.slice(0, lastChar) : path
};

const suffixPath = path => {
  return path[0] === '/' ? path.slice(1) : path
};

const combinePath = (prefix, suffix) => `${prefixPath(prefix)}/${suffixPath(suffix)}`;

const proxy = ({ base, path, rewrite }) =>
  createProxyMiddleware({
    target: combinePath(base, path),
    pathRewrite: {
      // Shortening paths to avoid duplicate paths
      // Current path to which the request is getting send persists - "/api/[resource]" gets appended with "/api/[resource]/[extra]"
      //  -> Strip away base to avoid confusion => "/api/[resource]/[extra]"
      [`^${rewrite}`]: "/",
    },
    timeout: PROXY_TIMEOUT,
    onProxyReq(proxyReq, req, res) {
      if (!req.body || !Object.keys(req.body).length) {
        return;
      }

      const contentType = proxyReq.getHeader('Content-Type');
      const writeBody = (bodyData) => {
        proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
        proxyReq.write(bodyData);
      };

      if (contentType === 'application/json') {
        writeBody(JSON.stringify(req.body));
      }

      if (contentType === 'application/x-www-form-urlencoded') {
        writeBody(querystring.stringify(req.body));
      }
    }
  });

const clientProxyDecorator = (client, { url: base }) => {
  client.proxy = (path, apiPath = null) => proxy({
    base,
    path,
    rewrite: apiPath || path
  });

  return client;
};

module.exports = { clientProxyDecorator };