const { createRedisClient } = require("./redisClient");
const { provisionOperations } = require("../common");

const createRedisDatasource = (name, redisConfig) => {
  const client = createRedisClient(redisConfig);

  return {
    name,
    query(query) {
      return query(client);
    },
    repository(repo) {
      return provisionOperations(client, repo.operations);
    }
  }
};

module.exports = { createRedisDatasource };
