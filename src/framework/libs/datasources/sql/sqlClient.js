const createKnex = require("knex");

const knexConfig = config => ({
  client: "pg",
  connection: config,
  pool: {
    min: 2,
    max: 10
  }
});

const createSQLClient = config => createKnex(knexConfig(config));

module.exports = { createSQLClient };
