const Queue = require("bull");
const debug = require("debug");

const taskLogger = debug("app").extend("system").extend("tasks");

const createQueue = ({ name, config }) => {
  // Separating for additional namespaces
  const queue = new Queue(name, config); // Important: Queues are cheap

  queue.on("completed", job => {
    taskLogger(`Task "${job.name}" COMPLETED on queue "${name}" with data`, job.data);
  });

  return {
    createTask(taskName, params, processor) {
      queue.process(taskName, async job => {
        try {
          taskLogger(`Task "${job.name}" is being PROCESSED on queue "${name}" with data`, job.data);

          await Promise.resolve(processor(job.data || {}));
        } catch (e) {
          taskLogger(`Task "${job.name}" FAILED on queue "${name}" with data`, job.data);
        }
      });

      return {
        async run(data) {
          await queue.add(taskName, data, params)
        }
      };
    }
  };
}

module.exports = { createQueue }; 
