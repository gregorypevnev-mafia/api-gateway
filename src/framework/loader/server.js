const { createServer } = require("../libs/server");

const loadServer = ({
  port,
  host,
  secure
}) => {
  let app = null, wss = null;

  return {
    useApi(api) {
      app = api;
    },
    useWs(ws) {
      wss = ws;
    },
    build(cb) {
      return createServer(app, wss, { port, host, secure }, cb);
    }
  }
};

module.exports = { loadServer };
