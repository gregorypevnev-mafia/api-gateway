const flatten = data =>
  Array.isArray(data) ? data.reduce((all, current) => (
    [...all, ...flatten(current)]
  ), []) : [data];

module.exports = { flatten };
