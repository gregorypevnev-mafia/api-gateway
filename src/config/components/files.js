const DEFAULT_SIGNING_SECRET = "SECRET";
const SIGNING_SECRET = String(process.env.SIGNING_SECRET || DEFAULT_SIGNING_SECRET);

const DEFAULT_SIGNATURE_TTL = 60 * 1000;
const SIGNATURE_TTL = Number(process.env.SIGNATURE_TTL || DEFAULT_SIGNATURE_TTL);

const DEFAULT_TEMP_FILES = "./files";
const TEMP_FILES = String(process.env.TEMP_FILES || DEFAULT_TEMP_FILES);

module.exports = {
  path: TEMP_FILES,
  secret: SIGNING_SECRET,
  ttl: SIGNATURE_TTL,
  allowedExtensions: ["jpg", "png"],
};
