const os = require("os");

const NAME = "api-gateway";
const HOST = String(os.hostname());
const ID = Number(process.pid);

module.exports = {
  name: NAME,
  host: HOST,
  id: ID,
};