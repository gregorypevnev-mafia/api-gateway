const DEFAULT_GAMES_SERVICE_URL = "http://localhost:3001";
const GAMES_SERVICE_URL = String(process.env.GAMES_SERVICE_URL || DEFAULT_GAMES_SERVICE_URL);

module.exports = {
  type: "service",
  config: {
    url: GAMES_SERVICE_URL,
    timeout: 1000,
    circuitBreaker: {
      failureCount: 3,
      failureTimeout: 5000,
      retriesCount: 2,
      retriesTimeout: 100,
    }
  }
};