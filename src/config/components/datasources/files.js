const DEFAULT_FILES_SERVICE_URL = "http://localhost:3002";
const FILES_SERVICE_URL = String(process.env.FILES_SERVICE_URL || DEFAULT_FILES_SERVICE_URL);

module.exports = {
  type: "service",
  config: {
    url: FILES_SERVICE_URL,
    timeout: 1000,
    circuitBreaker: {
      failureCount: 3,
      failureTimeout: 5000,
      retriesCount: 2,
      retriesTimeout: 100,
    }
  }
};