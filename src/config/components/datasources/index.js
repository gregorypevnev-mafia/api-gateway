module.exports = {
    users: require('./users'),
    games: require('./games'),
    files: require('./files')
};