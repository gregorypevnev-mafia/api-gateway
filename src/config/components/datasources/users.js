const DEFAULT_USERS_SERVICE_URL = "http://localhost:3000";
const USERS_SERVICE_URL = String(process.env.USERS_SERVICE_URL || DEFAULT_USERS_SERVICE_URL);

module.exports = {
  type: "service",
  config: {
    url: USERS_SERVICE_URL,
    timeout: 1000,
    circuitBreaker: {
      failureCount: 3,
      failureTimeout: 5000,
      retriesCount: 2,
      retriesTimeout: 100,
    }
  }
};