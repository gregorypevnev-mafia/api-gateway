const DEFAULT_SECRET = "SECRET";
const SECRET = String(process.env.SECRET || DEFAULT_SECRET);

const DEFAULT_STORE_HOST = "localhost";
const STORE_HOST = String(process.env.STORE_HOST || DEFAULT_STORE_HOST);

const DEFAULT_STORE_PORT = 8000;
const STORE_PORT = String(process.env.STORE_PORT || DEFAULT_STORE_PORT);

const DEFAULT_STORE_PASSWORD = "pass";
const STORE_PASSWORD = String(process.env.STORE_PASSWORD || DEFAULT_STORE_PASSWORD);

module.exports = {
  tokens: {
    algorithm: "HS256",
    audience: "APP",
    issuer: "SYSTEM",
    secret: SECRET,
    session: 24 * 60 * 60, // Seconds, NOT milliseconds
  },
  http: {
    header: "Authorization",
    token: "JWT",
  },
  store: {
    host: STORE_HOST,
    port: STORE_PORT,
    password: STORE_PASSWORD
  }
};