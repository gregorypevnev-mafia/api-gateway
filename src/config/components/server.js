const DEFAULT_PORT = 3003;
const PORT = Number(process.env.PORT || DEFAULT_PORT);

const USE_SSL = !!process.env.USE_SSL;

module.exports = {
  port: PORT,
  host: "0.0.0.0",
  secure: USE_SSL,
};