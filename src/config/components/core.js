const DEFAULT_KAFKA_HOST = "localhost:9091";
const KAFKA_HOST = String(process.env.KAFKA_HOST || DEFAULT_KAFKA_HOST);

module.exports = {
  events: {
    mq: {
      id: "file-storage",
      host: KAFKA_HOST,
    },
  },


  tasks: { queue: null }
}
