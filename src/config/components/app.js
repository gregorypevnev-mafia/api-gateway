const API_GATEWAY_ID = "API_GATEWAY_ID"; // Could be extended with permissions

const DEFAULT_IMAGE = "default.png";

module.exports = {
  user: API_GATEWAY_ID,
  tokens: require("./api/auth").tokens,
  files: require("./files"),
  defaultImage: DEFAULT_IMAGE,
};
