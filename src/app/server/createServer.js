const express = require("express");
const parser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");

const createServer = () => {
  const app = express();

  app.use(morgan("dev"));
  app.use(cors({
    origin: "*",
    allowedHeaders: "*",
    methods: ["GET", "POST", "DELETE", "PUT", "OPTIONS", "HEAD"],
    exposedHeaders: "*"
  }));
  app.use(parser.json());
  app.use(helmet());

  return app;
};

module.exports = createServer;