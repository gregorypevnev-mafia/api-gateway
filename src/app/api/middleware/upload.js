const multer = require("multer");
const pathUtil = require("path");

const name = "upload";

const extension = file => pathUtil.extname(file.originalname).slice(1);

const uniqueName = file => `${Date.now()}.${extension(file)}`;

const handler = ({
  config: {
    files: {
      allowedExtensions,
      path
    }
  }
}) => ({ field }) => {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path)
    },
    filename: function (req, file, cb) {
      cb(null, uniqueName(file))
    }
  });

  const upload = multer({
    storage,
    fileFilter(req, file, cb) {
      const isAllowed = allowedExtensions.indexOf(extension(file)) !== -1;

      cb(null, isAllowed);
    }
  });

  return upload.single(field);
};

module.exports = {
  name,
  handler,
};