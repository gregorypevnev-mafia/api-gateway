const dashboard = ({ queries }) => async (req, res) => {
  const friendsData = await queries.query("friends", { token: req.token });

  return res.status(200).json(friendsData);
};

const proxy = ({
  repositories: { users }
}) => users.friendsResource();

module.exports = {
  path: "/friends",
  middleware: [],
  auth: true,
  private: true,
  protected: false,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "GET",
        controller: dashboard,
      }
    },
    {
      path: "/",
      middleware: [],
      use: {
        method: "ALL",
        controller: proxy,
      }
    }
  ],
};
