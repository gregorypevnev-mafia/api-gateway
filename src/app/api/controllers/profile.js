const info = ({ queries }) => async (req, res) => {
  const { user } = await queries.query("profile", {
    user: req.user,
    token: req.token,
  });

  return res.status(200).json({ user });
};

module.exports = {
  path: "/profile",
  middleware: [],
  auth: true,
  private: true,
  protected: false,
  use: {
    method: "GET",
    controller: info,
  }
};
