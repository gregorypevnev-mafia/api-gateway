const proxy = ({
  repositories: { games }
}) => games.invitationsResource();

module.exports = {
  path: "/invitations",
  middleware: [],
  auth: true,
  private: true,
  protected: false,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "ALL",
        controller: proxy,
      }
    },
  ],
};
