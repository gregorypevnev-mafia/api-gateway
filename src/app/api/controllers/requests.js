const proxy = ({
  repositories: { users }
}) => users.requestsResource();

module.exports = {
  path: "/requests",
  middleware: [],
  auth: true,
  private: true,
  protected: false,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "ALL",
        controller: proxy,
      }
    },
  ],
};
