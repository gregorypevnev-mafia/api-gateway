const dashboard = ({ queries }) => async (req, res) => {
  const gamesData = await queries.query("games", { token: req.token });

  return res.status(200).json(gamesData);
};

const proxy = ({
  repositories: { games }
}) => games.gamesResource();

module.exports = {
  path: "/games",
  middleware: [],
  auth: true,
  private: true,
  protected: false,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "GET",
        controller: dashboard,
      }
    },
    {
      path: "/",
      middleware: [],
      use: {
        method: "ALL",
        controller: proxy,
      }
    },
  ],
};
