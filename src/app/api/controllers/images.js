const serve = ({
  repositories: { files }
}) => files.filesResource();

module.exports = {
  path: "/images/:image",
  middleware: [],
  use: {
    method: "GET",
    controller: serve,
  }
};
