const dashboard = ({ queries }) => async (req, res) => {
  const queryData = {
    user: req.user,
    token: req.token,
  };

  const [profile, friends, games] = await Promise.all([
    queries.query("profile", queryData),
    queries.query("friends", queryData),
    queries.query("games", queryData),
  ]);

  return res.status(200).json({
    profile,
    friends,
    games,
  });
};

module.exports = {
  path: "/dashboard",
  middleware: [],
  auth: true,
  private: true,
  protected: false,
  use: {
    method: "GET",
    controller: dashboard,
  },
};
