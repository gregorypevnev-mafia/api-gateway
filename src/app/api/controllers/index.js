module.exports = {
  path: "/",
  use: [
    require("./games"),
    require("./friends"),
    require("./invitations"),
    require("./requests"),
    require("./profile"),
    require("./dashboard"),
    require("./images"),
  ],
};