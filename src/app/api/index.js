module.exports = {
  controller: require("./controllers"),
  middleware: require("./middleware"),
};