const name = "users";
const datasource = "users";
const transactional = false;

const checkUser = axios => async (username, password) => {
  const checkData = {
    username,
    password,
  };

  try {
    const { data } = await axios({
      url: "/api/users/check",
      method: "POST",
      data: checkData,
    });

    return data;
  } catch (e) {
    const { status, data } = e.response;

    throw { status, data };
  }
}

const createUser = axios => async userData => {
  try {
    const { data } = await axios({
      url: "/api/users",
      method: "POST",
      data: userData
    });

    return data;
  } catch (e) {
    const { status, data } = e.response || {};

    throw { status, data };
  }
}

const friendsResource = axios => () => axios.proxy("/api/friends");

const requestsResource = axios => () => axios.proxy("/api/requests");

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    checkUser,
    createUser,
    friendsResource,
    requestsResource,
  },
};