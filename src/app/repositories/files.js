const { requestHeaders } = require("../utils/requests");
const FormData = require("form-data");

const name = "files";
const datasource = "files";
const transactional = false;

const formData = ({ file }) => {
  const form = new FormData();

  form.append("file", file);

  return form;
};

const signedURL = ({ signature, expires }) => `/api/files?signature=${signature}&expires=${expires}`;

const filesResource = axios => () => axios.proxy("/api/files", "/api/images");

const uploadHeaders = (token, form) => ({
  ...requestHeaders(token),
  ...form.getHeaders()
  // IMPORTANT: Retrieving Headers from Form-Data - Includes size related information
  //  -> "Content-Type: multipart/form-data; boudary=???" - Boundary is calculated by Form-Data internally
  //  => Necessary for reading files and fields
})

const upload = axios => async (file, token, signedData) => {
  const url = signedURL(signedData);
  const data = formData({ file });
  const headers = uploadHeaders(token, data);

  const result = await axios({
    url,
    headers,
    data,
    method: "POST",
  });

  return result.data;
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    filesResource,
    upload,
  },
};