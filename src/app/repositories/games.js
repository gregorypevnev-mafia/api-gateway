const name = "games";
const datasource = "games";
const transactional = false;

const gamesResource = client => () => client.proxy("/api/games");

const invitationsResource = client => () => client.proxy("/api/invitations");

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    gamesResource,
    invitationsResource,
  },
};