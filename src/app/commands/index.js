module.exports = [
    require('./sign-up'),
    require('./sign-in'),
    require('./publish-image')
];