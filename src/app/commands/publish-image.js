const type = "publish-image";

const handler = ({
  config: { defaultImage },
  repositories: { files: filesRepo },
  services: { signer, tokens, files: filesService }
}) => async ({ image }) => {
  console.log("IMAGE", image);

  try {
    const file = filesService.file(image);
    const token = tokens.generate();
    const signedData = signer.sign({ file: image, user: token });

    const { filename } = await filesRepo.upload(file, token, signedData);

    return filename || defaultImage;
  }
  catch (e) {
    if (image) await filesService.clear(image);

    return defaultImage;
  }
};

module.exports = {
  type,
  handler
};