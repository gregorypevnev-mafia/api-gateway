const type = "sign-in";

const handler = ({
  errors,
  repositories: { users }
}) => async ({ username, password }) => {
  const { user } = await users.checkUser(username, password);

  return user;
};

module.exports = {
  type,
  handler
};