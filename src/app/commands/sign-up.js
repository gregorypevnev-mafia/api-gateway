const type = "sign-up";

const handler = ({
  repositories: { users: usersRepo },
}) => async ({ username, password, image }) => {
  const { user } = await usersRepo.createUser({
    username,
    password,
    image,
  });

  return user;
};

module.exports = {
  type,
  handler
};