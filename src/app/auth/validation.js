const userDataSchema = require("../schemas/user-data");

const validateUserData = async data => {
  try {
    const result = await userDataSchema.validate(data, { abortEarly: false });

    return result;
  } catch (e) {
    throw {
      validation: true,
      errors: e.errors,
    }
  }
}

module.exports = { validateUserData };
