const { validateUserData } = require("./validation");

exports.findUser = ({ commands }) => async ({ username, password }) => {
  const user = await commands.call("sign-in", { username, password });

  return user;
};

exports.createUser = ({
  commands
}) => async data => {
  try {
    const { image: profileImage, ...userData } = await validateUserData(data);

    const image = await commands.call("publish-image", { image: profileImage });

    const user = await commands.call("sign-up", { ...userData, image });

    return user;
  } catch (e) {
    console.log(e);

    if (e.validation)
      throw {
        data: {
          message: "Invalid input",
          errors: e.errors
        },
        status: 400,
      };

    throw e;
  }
};

exports.detailsForUser = ({ }) => user => user; // No additional info (Using profile anyway)
