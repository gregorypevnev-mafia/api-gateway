const { createSigner } = require("../../framework/libs/api/files/signer/sign");

const name = "signer";

const sign = ({ files }) => {
  const signFile = createSigner(files);

  return ({ file, user }) => signFile(file, user);
};

module.exports = {
  name,
  functions: {
    sign
  }
};