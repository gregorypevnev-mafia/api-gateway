module.exports = [
    require('./signer'),
    require('./tokens'),
    require('./files')
];