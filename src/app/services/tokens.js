const { encodeToken } = require("../../framework/libs/api/auth/utils/tokens");

const name = "tokens";

const generate = ({ user, tokens }) => {
  const encode = encodeToken(tokens);

  return () => encode({ id: user });
}

module.exports = {
  name,
  functions: {
    generate,
  }
};