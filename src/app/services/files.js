const fsUtil = require("fs");
const pathUtil = require("path");

const name = "files";

const file = ({ files: { path } }) => filename =>
  fsUtil.createReadStream(pathUtil.join(path, filename));

const clear = ({ files: { path } }) => filename =>
  new Promise((res, rej) => {
    fsUtil.unlink(pathUtil.join(path, filename), err => {
      if (err) rej(err);
      else res();
    })
  });

module.exports = {
  name,
  functions: {
    file,
    clear,
  }
};