const requestHeaders = token => ({
  "Authorization": `JWT ${token}`
});

module.exports = { requestHeaders };
