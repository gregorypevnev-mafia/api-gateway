const { requestHeaders } = require("../utils/requests");

const type = "games";
const datasource = "games";

const handler = axios => () => async ({
  token,
}) => {
  const headers = requestHeaders(token);

  const [
    { data: { games } },
    { data: { invitations } }
  ] = await Promise.all([
    axios({
      url: "/api/games",
      method: "GET",
      headers,
    }),
    axios({
      url: "/api/invitations",
      method: "GET",
      headers,
    })
  ]);

  return {
    games,
    invitations,
  };
};

module.exports = {
  type,
  datasource,
  handler,
};