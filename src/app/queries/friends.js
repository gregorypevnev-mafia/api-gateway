const { requestHeaders } = require("../utils/requests");

const type = "friends";
const datasource = "users";

const handler = axios => () => async ({
  token,
}) => {
  const headers = requestHeaders(token);

  const [
    { data: { friends } },
    { data: { requests } }
  ] = await Promise.all([
    axios({
      url: "/api/friends",
      method: "GET",
      headers,
    }),
    axios({
      url: "/api/requests",
      method: "GET",
      headers,
    })
  ]);

  return {
    friends,
    requests,
  };
};

module.exports = {
  type,
  datasource,
  handler,
};