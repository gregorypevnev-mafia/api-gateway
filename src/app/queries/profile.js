const type = "profile";
const datasource = "users";

const handler = axios => () => ({
  user: { id },
  token,
}) =>
  axios({
    url: `/api/users/${id}`,
    method: "GET",
    headers: {
      "Authorization": `JWT ${token}`
    }
  }).then(({ data }) => data)

module.exports = {
  type,
  datasource,
  handler,
};