const yup = require("yup");

const userDataSchema = yup.object({
  username: yup.string()
    .min(3, "Username is too short (At least 3 characters)")
    .max(20, "Username is too long (At most 20 characters)")
    .required("Username is required"),

  password: yup.string()
    .min(6, "Password is too short (At least 6 characters)")
    .max(50, "Password is too long (At most 50 characters)")
    .required("Password is required"),

  image: yup.string().nullable(true)
});

module.exports = userDataSchema;
