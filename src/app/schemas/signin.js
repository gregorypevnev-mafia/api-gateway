const yup = require("yup");

const name = "signin";

const schema = yup.object({
  username: yup.string().required("Username must be provided"),
  password: yup.string().required("Password must be provided")
});

module.exports = {
  name,
  schema,
};