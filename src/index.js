const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./app/events.json"))
    .addCommands(require("./app/commands"))
    .addQueries(require("./app/queries"))
    .addRepositories(require("./app/repositories"))
    .addServices(require("./app/services"))
    .withApi(require("./app/api"))
    .withUsers(require("./app/auth/users"))
    .withSchemas(require("./app/schemas"))
    .withServer(require("./app/server"))
    .useInfo()
    .build();

  microservice.start();
}());
