#!/bin/bash

# Generating Key for generating Private-Key
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048

# Generating Private-Key (Decrypting session-data sent by Clients for establishing sessions)
openssl rsa -passin pass:x -in server.pass.key -out server.key

# Generaing Certificate-Signing-Request from Private-Key
#  - Metadata and Private-Key information used for generating Certificate by Certificate-Authorities
#  -> Used for generating Certificate (Either by Certificate-Authority or Manually) 
openssl req -new -key server.key -out server.csr

# Generating Certificate from Certificate-Signing-Request - Could have been done be Certificate-Authority
#  - Acts as a Public-Key for the Server's Private-Key 
#  -> Used by clients for
#    1) Encrypting session-data for establishing sessions wit the Server
#    2) Verifying that the certificate can be trusted through Certificate-Authortiies
#  Note: Since this is a self-signed certificate, it won't be trusted (Not a problem for personal projects)
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt

# Cleaning Up
rm server.pass.key
rm server.csr